package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.ApplicationUser;
import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.ApplicationUserRepository;
import ch.zli.m223.punchclock.repository.EntryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntryService {
    private EntryRepository entryRepository;
    private ApplicationUserRepository applicationUserRepository;

    public EntryService(EntryRepository entryRepository, ApplicationUserRepository applicationUserRepository) {
        this.entryRepository = entryRepository;
        this.applicationUserRepository = applicationUserRepository;
    }

    public Entry createEntry(Entry entry, String username) {
        ApplicationUser user = applicationUserRepository.findByUsername(username);
        entry.setApplicationUser(user);
        return entryRepository.saveAndFlush(entry);
    }

    public void deleteEntry(long id) { entryRepository.deleteById(id); }

    public Entry updateEntry(Entry entry) {
        return entryRepository.save(entry);
    }

    public List<Entry> findAll() {
        return entryRepository.findAll();
    }
}
