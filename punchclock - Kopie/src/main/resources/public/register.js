const URL = 'http://localhost:8081';

//POST-Request zum Registrieren eines Users.
function onRegister() {
    fetch(`${URL}/users/sign-up`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: document.getElementById('username').value,
            password: document.getElementById('password').value
        })
    }).then((result) => {
        if (result.ok) {
            window.location.replace('../')
        }
    });
}