const URL = 'http://localhost:8081';
let entries = [];
const TOKEN = localStorage.getItem("Token");

window.onload = function() {
    document.getElementById("loginSound").play();
}

const dateAndTimeToDate = (dateString, timeString) => {
    return new Date(`${dateString}T${timeString}`).toISOString();
};

const createEntry = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const entry = {};
    entry['checkIn'] = dateAndTimeToDate(formData.get('checkInDate'), formData.get('checkInTime'));
    entry['checkOut'] = dateAndTimeToDate(formData.get('checkOutDate'), formData.get('checkOutTime'));

    if(entry['checkIn'] < entry['checkOut']) {
        fetch(`${URL}/entries`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : TOKEN
            },
            body: JSON.stringify(entry)
        }).then((result) => {
            result.json().then((entry) => {
                entries.push(entry);
                renderEntries();
            });
        });
    }else{
        alert("Check out can't be before check in")
    }
};


const deleteEntry = (entry) => {
    fetch (`${URL}/entries/${entry.id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : TOKEN
        },
    }).then(() => {
        indexEntries();
    })
}

const updateEntry = (id) => {
    const checkIn = document.querySelector(`#checkIn${id}`);
    const checkOut = document.querySelector(`#checkOut${id}`);

    const entry = {};
    entry['checkIn'] = checkIn.value;
    entry['checkOut'] = checkOut.value;
    entry['id'] = id;

    fetch(`${URL}/entries/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : TOKEN
        },
        body: JSON.stringify(entry)
    }).then(() => {
        indexEntries();
    });
}

const indexEntries = () => {
    fetch(`${URL}/entries`, {
        method: 'GET',
        headers: {
            'Authorization' : TOKEN
        }
    }).then((result) => {
        result.json().then((result) => {
            entries = result;
            renderEntries();
        });
    });
    renderEntries();
};

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const createDeleteButton = (text, entry) => {
    const cell = document.createElement('button');
    cell.innerText = text;
    cell.addEventListener("click", () => deleteEntry(entry))
    return cell;
};

const createUpdateButton = (text, id) => {
    const cell = document.createElement('button');
    cell.innerText = text;
    cell.addEventListener("click", () => updateEntry(id))
    return cell;
};

const createDateTimeInput = (value, id) => {
    const input = document.createElement('input');
    input.type = 'datetime-local';
    input.value = value;
    input.id = id;
    return input;
};

const renderEntries = () => {
    const display = document.querySelector('#entryDisplay');
    display.innerHTML = '';
    entries.forEach((entry) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(entry.id));
        row.appendChild(createCell('')).appendChild(createDateTimeInput(entry.checkIn, `checkIn${entry.id}`));
        row.appendChild(createCell('')).appendChild(createDateTimeInput(entry.checkOut, `checkOut${entry.id}`));
        row.appendChild(document.createElement("td")).appendChild(createDeleteButton("Delete", entry));
        row.appendChild(document.createElement("td")).appendChild(createUpdateButton("Save", entry.id));
        display.appendChild(row);
    });
};

document.addEventListener('DOMContentLoaded', function(){
    const createEntryForm = document.querySelector('#createEntryForm');
    createEntryForm.addEventListener('submit', createEntry);
    indexEntries();
});

