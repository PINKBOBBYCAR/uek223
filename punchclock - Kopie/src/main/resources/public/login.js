const URL = 'http://localhost:8081';

//POST-Request zum Einloggen eines Users.
function onLogin() {
    fetch(`${URL}/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: document.getElementById('username').value,
            password: document.getElementById('password').value
        })
    }).then((result) => {
        if (result.ok) {
            localStorage.setItem("Token", result.headers.get('Authorization'))
            window.location.replace('../punchclock.html')
        }
    });
}